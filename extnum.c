#include <stdio.h>
#include <string.h>

int get_number_of_digit(long n){
  int ans = 1;

  while((n /= 10) > 0)
    ans++;

  return ans;
}

// Extract numbers from a line and print them
main(){
  char buff[1024];

  while(fgets(buff, sizeof(buff), stdin)){
    int i;

    for(i=0;i<strlen(buff);){
      if(isdigit(buff[i])){
	printf("%ld ", atol(buff+i));
	i += get_number_of_digit(atol(buff+i));
      }
      else
	i++;
    }

    printf("\n");
  }
}
